import React, { Fragment, useState } from 'react';
import Persons from './components/Person/Persons';
import { toast, ToastContainer } from 'react-toastify';
import Header from './components/common/Header';
import SimpleContext from './context/SimpleContext';
import { Button } from 'react-bootstrap';
import NewPerson from './components/Person/NewPerson';

export default function App () {
  //useState
  const [getPersons, setPersons] = useState([]);
  const [getSinglePerson, setSinglePerson] = useState(' ');
  const [getShowPersons, setShowPersons] = useState(true);

  const handleShowPerson = () => {
    setShowPersons(!getShowPersons);
  };

  const handleDeletePerson = id => {
    //filter
    const persons = [...getPersons];
    const filteredPersons = persons.filter(p => p.id !== id);
    setPersons(filteredPersons);

    //toast
    const personIndex = persons.findIndex(p => p.id === id);
    const person = persons[personIndex];
    toast.error(`${person.fullname} با موفقیت حذف شد`, {
      position: 'top-right',
      closeOnClick: true
    });
  };

  const handleNameChange = (event, id) => {
    const allPersons = getPersons;

    const personIndex = allPersons.findIndex(p => p.id === id);
    const person = allPersons[personIndex];
    person.fullname = event.target.value;
    console.log('handlenamechange');

    const persons = [...allPersons];

    persons[personIndex] = person;
    setPersons(persons);
  };

  const handleNewPerson = () => {
    const persons = [...getPersons];
    const person = {
      id: Math.floor(Math.random() * 1000),
      fullname: getSinglePerson
    };

    if (person.fullname !== '' && person.fullname !== ' ') {
      persons.push(person);
      setPersons(persons);
      setSinglePerson(' ');

      //TOAST
      toast.success('فردی با موفقیت اضافه شد.', {
        position: 'bottom-right',
        closeButton: true,
        closeOnclick: true
      });
    }
  };

  const setPerson = event => {
    setSinglePerson(event.target.value);
  };

  let person = null;

  if (getShowPersons) {
    person = <Persons />;
  }

  return (
    <SimpleContext.Provider
      value={{
        persons: getPersons,
        person: getSinglePerson,
        handleDeletePerson: handleDeletePerson,
        handleNameChang: handleNameChange,
        handleNewPerson: handleNewPerson,
        setPerson: setPerson
      }}
    >
      <Fragment>
        <div className='rtl text-center'>
          <Header appTitle='مدیریت کننده اشخاص' />

          <NewPerson />

          <Button
            onClick={handleShowPerson}
            variant={getShowPersons ? 'info' : 'danger'}
          >
            نمایش اشخاص
          </Button>


          {person}
          <ToastContainer />
        </div>
      </Fragment>
    </SimpleContext.Provider>
  );
}