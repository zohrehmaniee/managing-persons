
import { createContext } from 'react';

const SimpleContext = createContext ({
persons:[],
  person:'',
  handleDeletePerson: () => {},
  handleNameChang: () => {},
  handleNewPerson: () => {},
  setPerson: () => {}
});
export default SimpleContext;
